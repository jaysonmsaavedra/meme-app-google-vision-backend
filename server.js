const express = require('express')
const multer = require('multer')
const cors = require('cors')
const fs = require('fs')
const detectText = require('./detectText')

const app = express()
const upload = multer({ dest: 'images/'})
app.use(cors())

app.post('/image', upload.single('meme'), async (req, res) => {
    await console.log(`${req.file.originalname} received`)
    let text = await detectText(`images/${req.file.filename}`)
    await res.json({success: true, text })
})

app.listen(3001, () => {
    console.log('listening on port 3001')
})