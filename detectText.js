const vision = require('@google-cloud/vision')

const client = new vision.ImageAnnotatorClient()

const detectText = filePath => {
    return client.textDetection(filePath)
        .then(res => {
            return res[0].fullTextAnnotation.text
        })
        .catch(console.error)
}

module.exports = detectText